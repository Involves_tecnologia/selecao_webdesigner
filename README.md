Seleção Desenvolvedor PHP (pleno)
==

Olá, estamos muito felizes pelo seu interesse em fazer parte da nossa equipe. Gostaríamos de conhecer um pouco mais das suas habilidades como desenvolvedor PHP e, para isso, preparamos o teste abaixo.

Sobre o teste
==

O teste consiste em construir e codificar uma landing page de cadastro para o tradecast, que é um webinar quinzenal do portal Clube do Trade que traz nomes do trade marketing para debater assuntos da área.
Para isso você deve utilizar a ferramenta de sua preferência para construir uma nova landing page (Photoshop, Illustrator, Gimp) e codificar a interface utilizando html e css. As informações estão no arquivo tradecast.json abaixo.



Aqui está o link do portal Clube do Trade: https://clubedotrade.com.br/

e também uma landing page de exemplo: https://clubedotrade.com.br/tradecast-24-cadastro/
 
 
Sobre a avaliação
--

Para fins de avaliação, será levado em consideração os seguintes quesitos:

- criatividade
- codificação html/css

Bônus:

- HTML 5
- Responsividade

Observações 

- Não é necessário ter nenhuma validação de dados ou envio de formulário
- Você pode enviar um zip do seu projeto contendo o código fonte ou link do repositório para talentos@involves.com.br com o título "[teste-webdesigner-2017-05] <nome_do_candidato>" até a data limite informada no email em que você recebeu o link para esse teste.